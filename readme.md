
## Magento Test

Version: Magento 2.2.x - 2.3.x

### Backend tasks - each task should be a seperate module:
- Create programmatically product attributes
    * Note -> textfield
    * Ram size -> select with options 8gb, 16gb and 

- Create programmatically customer attributes
    * Password field -> textfield showing only to the admin area. Not to the front end customer
    * When an admin user saves a customer if the Password field from (i) has value update the password for this customer
		
- Create a cli command to import  products by adding values for the attributes from (1). 
    * Four products as simple products (Use any data you want)
    * Create a configurable product by attaching the simple products from (i).
    * Create a bundle product by attaching the simple products from (i).
    * All products should have at least an image.

 - Create an API endpoint for exporting orders with the following format `rest/V1/orders/export`. Use magento's capabilities to create the endpoint.
 
### Frontend tasks

- Create a custom theme by inheriting the default Luma theme. Just the bare bone theme without any files yet.

- Create a custom page 
    * Url: my/custom/page
    * 3 columns layout

- Create a custom block or widget
    * Showing the imported simple products as a carusel. Use whatever jquery plugin you like to create the carusel effect
    * Place the block/widget from (i) to every page on the left sidebar
    * Place the block/widget from (i) to the custom created page from (2) bellow the main content area.


### Deliveries
Use git version control to present your workflow completing the above tasks. Be as descriptive as possible to your commit messages.
We expect you to email us the link to your preferred git client (github, bitbucket, gitlab) with guides on how to setup the project in the README file.

Have fun!!
